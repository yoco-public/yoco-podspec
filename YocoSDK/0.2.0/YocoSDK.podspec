Pod::Spec.new do |s|
  s.name             = 'YocoSDK'
  s.version          = '0.2.0'
  s.summary          = 'Yoco Payments SDK'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/yoco-public/yoco-podspec'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Andrew Snowden' => 'andrew@yoco.com' }

  s.ios.deployment_target = '8.0'

  s.source_files = 'YocoSDK/Classes/**/*'

  s.source = {
      # For binary only.
#      :http => 'https://www.dropbox.com/s/xx6yw2klx98e1oj/testingYocoSDKPod.zip?dl=1',
      # For source only.
      :git => 'git@gitlab.com:yoco/yoco-sdk-pod.git', :tag => s.version.to_s,
  }
  # For binary only.
#  s.vendored_frameworks = 'YocoSDK.framework'

  # s.resource_bundles = {
  #   'YocoSDK' => ['YocoSDK/Assets/*.png']
  # }
  
  s.dependency 'payworks', '2.32'
  s.dependency 'Reachability'
  s.static_framework = true

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  
  s.prefix_header_contents = '#import "YCUtils.h"'
end
