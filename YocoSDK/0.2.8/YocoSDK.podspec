Pod::Spec.new do |s|
  s.name             = 'YocoSDK'
  s.version          = '0.2.8'
  s.summary          = 'Yoco Payments SDK'

  s.description      = <<-DESC
Base Yoco payments SDK - please use YocoSDK-UI to provide a complete card payment flow from within your application
                       DESC

  s.homepage         = 'https://developer.yoco.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Andrew Snowden' => 'andrew@yoco.com' }

  s.ios.deployment_target = '8.0'

  s.source_files = 'YocoSDK/Classes/**/*'

  s.source = {
      # For binary only.
#      :http => 'https://www.dropbox.com/s/xx6yw2klx98e1oj/testingYocoSDKPod.zip?dl=1',
      # For source only.
      :git => 'git@gitlab.com:yoco/yoco-sdk-pod.git', :tag => s.version.to_s,
  }
  # For binary only.
#  s.vendored_frameworks = 'YocoSDK.framework'

  # s.resource_bundles = {
  #   'YocoSDK' => ['YocoSDK/Assets/*.png']
  # }
  
#  s.public_header_files = 'YocoSDK/Classes/Utils/**/*.h', 'YocoSDK/Classes/Yoco/**/*.h'
  s.prefix_header_contents = '#import "YCUtils.h"'
  
  s.static_framework = true
  s.frameworks = 'UserNotifications'
  
  s.dependency 'Reachability'
  
  s.subspec 'NoDependencies' do |no_payworks|
  end
  
  s.subspec 'WithDependencies' do |payworks|
      payworks.dependency 'payworks', '2.32'
      payworks.dependency 'payworks/offline', '2.32'
  end
  
  s.default_subspec = 'WithDependencies'
end
