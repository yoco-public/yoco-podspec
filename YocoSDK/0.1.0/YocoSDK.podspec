Pod::Spec.new do |s|
  s.name             = 'YocoSDK'
  s.version          = '0.1.0'
  s.summary          = 'Yoco Payments SDK'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/yoco-public/yoco-podspec'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Andrew Snowden' => 'andrew@yoco.com' }

  s.ios.deployment_target = '8.0'

  #s.default_subspec = 'Source'
  s.source_files = 'YocoSDK/Classes'

  s.source = {
      #:http => 'https://www.dropbox.com/s/xx6yw2klx98e1oj/testingYocoSDKPod.zip?dl=1',
      :git => 'git@gitlab.com:yoco/yoco-sdk-pod.git', :tag => s.version.to_s,
  }
  #s.vendored_frameworks = 'YocoSDK'

  # s.resource_bundles = {
  #   'YocoSDK' => ['YocoSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
